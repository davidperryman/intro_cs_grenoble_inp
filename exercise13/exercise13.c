#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define N 30
#define LOWERBOUND 100
#define UPPERBOUND 150

int* init(int s) {
	// gen pseudo rand nums in lower-upper bound
	int i, *arr;
	arr = malloc(sizeof(int)*s);
	for (i=0;i<s;i++)
		arr[i] = rand()%(UPPERBOUND-LOWERBOUND) + LOWERBOUND;
	return arr;
}

int search(int x, int* t, int s) {
	// find element x in t
	int i;
	for (i=0;i<s;i++) if (t[i]==x) return i;
	return -1;
}

int main(int argc, char** argv) {
	int size, x, *arr, index, i;
	if (argc != 3) {
		printf("wrong arguments. supply an N for size of array and S for search term\n");
		return -1;
	}
	srand(time(NULL));	
	size = atoi(argv[1]);	
	x = atoi(argv[2]);
	arr = init(size);
	index = search(x, arr, size);
	printf("found integer %d in array", x);
	for (i=0; i<size; i++) printf(" %d", arr[i]);
	printf("\n");	
	printf("at index %d\n", index);	
}
