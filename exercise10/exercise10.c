#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	int i, num_even, input;
	if (argc != 2) {
		printf("wrong arguments. supply an N for the number of numbers\n");
		return -1;
	}
	num_even = 0;	
	for (i=0;i<atoi(argv[1]);i++) {
		printf("Give element index %d.\n", i);
		scanf("%d", &input);
		if (input%2 == 0) num_even++;	
	}
	printf("number of even inputs is %d\n", num_even);	
	return 0;
}
