#include <stdio.h>
#include <stdlib.h>

int is_even(int N) {
	if (N==0) return 1;	
	else if (N==1) return 0;
	return is_even(N-1);
}

int main(int argc, char** argv) {
	int input;
	if (argc != 2) {
		printf("wrong arguments. supply an N for is_even(N)\n");
		return -1;
	}
	input = atoi(argv[1]);	
	printf("computed is_even(%d) recursively as %d\n", input, is_even(input));
}
