#include <stdio.h>
#include <stdlib.h>

struct example {
	int field1;
	float field2;
};

void init1(int x, struct example *e) {
	e->field1 = x*2;
}

void init2(float x, struct example *e) {
	e->field2 = x + 10;
}

void initialization(int x, float y, struct example *e) {
	init1(x, e);
	init2(x, e);
}

void print_structure(struct example *e) {
	printf("In this structure, we have %d and %f\n", e->field1, e->field2);
}

struct example *s;

int main() {
	int i;
	float f;
	s = malloc(sizeof(struct example));	
	printf("which integer for the structure? ");
	scanf("%d", &i);
	printf("which float for the structure? ");
	scanf("%f", &f);
	initialization(i,f,s);
	printf("after initialization, the structure is as follows:\n");
	print_structure(s);
	return 0;
}
