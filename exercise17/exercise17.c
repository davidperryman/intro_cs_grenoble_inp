#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 200

int present(int k, int* s) {
	return s[k];
}

void remove2 (int k, int* s) {
	int incr = k;	
	k += incr;
	while (k<N) {
		s[k] = 0;
		k += incr;
	}
}
void build_sieve(int* sieve, int s, int n) {
	int i;
	for (i=0;i<n;i++) sieve[i] = 1;
	for (i=2;i<n;i++) if (sieve[i]) remove2(i, sieve);
}
int main(int argc, char** argv) {

	int sieve[N], x, k;
	printf("To which limit do you want to compute the prime numbers? ");
	scanf("%d", &x);
	//
	build_sieve(sieve, N, x);	
	//	
	printf("Here are the prime numbers up to %d : \n", x);
	for (k=0;k<x;k++)
		if (sieve[k]) printf("%d ", k);
	printf("\n");
	return 0;
}
