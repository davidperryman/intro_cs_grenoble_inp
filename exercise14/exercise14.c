#include <stdio.h>
#include <stdlib.h>

void printer(int* t,int x,int y,int* p) {
	int i;	
	printf("array t =");
	for (i=0;i<7;i++) printf(" %d", t[i]);	
	printf("\n");
	printf("x = %d\n", x);
	printf("y = %d\n", y);
	printf("p[0] = %d\n", p[0]);
	return;
}
int main(int argc, char** argv) {
	int t[7];
	int x = 5;
	int y = 36;
	int *p = t;	
	printer(t, x, y, p);	
	p = &x;
	printer(t, x, y, p);	
	*p = 371;
	printer(t, x, y, p);	
	t = p;
	printer(t, x, y, p);	
	p = &y;
	printer(t, x, y, p);	
	return 0;
}
