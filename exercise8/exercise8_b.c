#include <stdio.h>

int main() {
	int x, y, sum;
	sum = 0;
	do {
		printf("Enter a starting value for the sum\n");
		scanf("%d", &x);	
		printf("Enter an ending value for the sum\n");
		scanf("%d", &y);	
	} while (x > y);
	for (;x<=y;x++) 
		sum = sum + x;
	printf("sum is %d\n", sum);	
	return 0;
}
