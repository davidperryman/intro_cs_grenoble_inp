#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	int i, input, total;
	if (argc != 2) {
		printf("wrong arguments. supply an N for N!\n");
		return -1;
	}
	input = atoi(argv[1]);
	total = 1;	
	for (i=1; i<=input; i++) {
		total *= i;	
		printf("%d! = %d\n", i, total);		
	}
	return 0;
}
