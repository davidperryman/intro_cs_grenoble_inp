# 1
echo "USER has this as it's value"
echo $USER
echo "HOME has this as it's value"
echo $HOME
echo "PWD has this as it's value"
echo $PWD

# 2
echo "/bin has this:"
echo $(ls /bin)

# 3
echo "ls is in /bin so it gets found when the shell searches through PATH to find an executable with the name ls"

# 4
echo "PATH has this as it's value"
echo $PATH
echo "PATH contains X values, they are "
echo $PATH

# 5
# after adding the path to the executable, welcome.sh works
