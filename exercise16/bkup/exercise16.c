#include <stdio.h>
#include <stdlib.h>


int min(float* arr, int size) {
	int i, min_index = 0;;	
	for (i=0;i<size;i++) if (arr[i]<arr[min_index]) min_index = i;
	return min_index;
}

int main(int argc, char** argv) {
	float *arr;
	int size, i;
	int mindex;	
	printf("How many elements do you want to insert in f_array? ");
	scanf("%d", &size);
	arr = malloc(sizeof(int)*size);
	for (i=0;i<size;i++) {
		printf("Give element number %d : ", i);
		scanf("%f", arr+i);
	}
	mindex = min(arr, size);	
	printf("The minimum is %f, located at the index %d\n", arr[mindex], mindex);
	return 0;
}
