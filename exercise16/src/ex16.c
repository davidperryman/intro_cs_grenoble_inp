#include <ex16_tools.h>

int main(int argc, char** argv) {
	float *arr;
	int size, i;
	int mindex;	
	printf("How many elements do you want to insert in f_array? ");
	scanf("%d", &size);
	arr = malloc(sizeof(int)*size);
	for (i=0;i<size;i++) {
		printf("Give element number %d : ", i);
		scanf("%f", arr+i);
	}
	mindex = min(arr, size);	
	printf("The minimum is %f, located at the index %d\n", arr[mindex], mindex);
	return 0;
}
