#include <stdio.h>
#include <stdlib.h>

int fact_rec(int N) {
	if (N==0) return 1;
	else return N*fact_rec(N-1);
}

int fact_it(int N) {
	int res;
	res = 1;	
	for (;N>0;N--) res *= N;	       
	return res;
}
int main(int argc, char** argv) {
	int input;
	if (argc != 2) {
		printf("wrong arguments. supply an N for N!\n");
		return -1;
	}
	input = atoi(argv[1]);	
	printf("computed %d! recursively as %d\n", input, fact_rec(input));
	printf("computed %d! iteratively as %d\n", input, fact_it(input));
}
