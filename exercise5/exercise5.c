#include <stdio.h>
#include <math.h>

#define EXPONENT 3

int main() {
	float nb, result;
	// user input
	printf("Give a real number\n");
	scanf("%f", &nb);
	// computation
	result = pow(nb, EXPONENT);

	// the result is displayed
	printf("%f to the power of %d is equal to %f\n", nb, EXPONENT, result);
	return 0;
}
