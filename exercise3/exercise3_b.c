#include <stdio.h>

int main() {
	int firstnumber, secondnumber, result;
	// user input
	printf("Give me the first number\n");
	scanf("%d", &firstnumber);
	printf("Give me the second number\n");
	scanf("%d", &secondnumber);
	// Computation of result
	result = firstnumber * secondnumber;
	// the result is displayed
	printf("The product of these values is : %d\n", result);
	return 0;
}	
