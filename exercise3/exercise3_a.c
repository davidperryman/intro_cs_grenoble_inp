#include <stdio.h>

int main() {
	float firstnumber, secondnumber, result;
	// user input
	printf("Give me the first number\n");
	scanf("%f", &firstnumber);
	printf("Give me the second number\n");
	scanf("%f", &secondnumber);
	// Computation of result
	result = firstnumber * secondnumber;
	// the result is displayed
	printf("The product of these values is : %f\n", result);
	return 0;
}	
